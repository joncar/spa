<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Archivo extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function get_header($crud,$cliente){
            $this->as[$this->router->fetch_method()] = 'clientes';
            $crud->header = $this->crud_function('','');
            $crud->header->set_subject('Cliente');
            $crud->header->where('clientes.id',$cliente);
            $crud->header->set_theme('header_data');
            $crud->header->set_url('archivo/clientes');
            $crud->header->columns('id','nombre','telefono','email','direccion','cumpleaños','referencia','total_sesiones');
            $crud->header->display_as('id','#Cliente');
            $crud->header->callback_column('total_sesiones',function($val,$row){
                return (string)$this->db->get_where('ventas',array('clientes_id'=>$row->id))->num_rows();
            });
            $crud->header = $crud->header->render(1)->output;
            return $crud;
        }
        
        function clientes(){
            $crud = $this->crud_function('',''); 
            $crud->unset_read()
                 ->unset_print()
                 ->unset_export();
            if($crud->getParameters()=='list'){
                $crud->unset_edit();
            }
            $crud->columns('#Cliente','nombre','telefono','email','direccion','cumpleanos','referencia');
            $crud->display_as('id','#Cliente')
                     ->display_as('telefono','Teléfono')
                     ->display_as('cumpleanos','Cumpleaños')
                     ->display_as('direccion','Dirección');
            $crud->callback_column('#Cliente',function($val,$row){
                return '<a href="'.base_url('archivo/clientes/edit/'.$row->id).'"><i class="fa fa-edit"></i> '.$row->id.'</a>';
            });
            $crud->callback_after_insert(function($post){
                get_instance()->enviarcorreo((object)$post,1);
            });
            //$crud->set_lang_string('insert_success_message','El cliente fue almacenado con éxito, si desea cargar productos para su venta puede hacerlo <a href="'.base_url('procesos/cargar_productos').'/{id}/add">AQUI</a> |');
            $this->loadView($crud->render());
        }
        
        function sucursales(){
            $crud = $this->crud_function('','');              
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        function productos($x = ""){
            $crud = $this->crud_function('',''); 
            $crud->field_type('grupo','dropdown',array('1'=>'Servicio','2'=>'Producto'))
                     ->field_type('user_id','hidden',$this->user->id)
                     ->field_type('cantidad_aplicaciones','string',1)
                     ->field_type('aplicaciones_realizadas','string',0)
                     ->field_type('tipo','dropdown',array('1'=>'Venta','2'=>'Uso'))
                     ->field_type('actualizacion','hidden',date("Y-m-d"));            
            $crud->callback_column('precio_venta',function($val,$row){                
                return $val;
            });
            $crud->callback_column('stock',function($val,$row){                
                if($row->grupo=='Servicio'){
                    return 'N/A';
                }
                return $val;
            });
            $crud->callback_before_update(function($post,$primary){
                $producto = get_instance()->db->get_where('productos',array('id'=>$primary))->row();
                if($post['stock']!=$producto->stock){
                    $tipo = $post['stock']<$producto->stock?-1:1;
                    get_instance()->db->insert('movimientos',array(
                        'productos_id'=>$primary,
                        'tipo'=>$tipo,
                        'motivo_movimiento'=>'Ajuste manual',
                        'cantidad'=>$_POST['addStock'],
                        'precio_compra'=>0,
                        'precio_venta'=>$post['precio_venta'],
                        'fecha'=>date("Y-m-d"),
                        'fecha_carga'=>date("Y-m-d"),
                        'user_id'=>get_instance()->user->id,
                        'sucursales_id'=>get_instance()->user->sucursal,
                    ));
                }
            });
            $crud->callback_column('precio_venta',function($val,$row){
                    return '$'.number_format($val,2,'.',',');
                });
            $crud->callback_column('actualizacion',function($val,$row){
                    return date("d/m/Y",strtotime($val)).'<br/>'.@$this->db->get_where('user',array('id'=>$row->user_id))->row()->nombre;
            });
            if($crud->getParameters()=='edit'){
                $crud->callback_field('stock',function($val,$row){
                        return form_input('addStock','','id="field-addStock" placeholder="Añadir stock"').form_input('stock',$val,'id="field-stock" readonly');
                });
            }
            $crud->unset_searchs('actualizacion');
            $crud->display_as('codigo','Código')
                 ->display_as('stock','Stock actual')
                 ->display_as('actualizacion','Última modificación')
                 ->display_as('sucursales_id','Sucursal');
            $crud->where('sucursales_id',$this->user->sucursal);
            $crud->columns('codigo','nombre','unidad_medida','precio_venta','grupo','min_stock','stock','sucursales_id','actualizacion');
            $crud->add_action('<i class="fa fa-arrows"></i> Movimientos','',base_url('archivo/movimientos/').'/');            
            $crud = $crud->render(); 
            $edit = $x=='edit'?TRUE:FALSE;
            $crud->output = $this->load->view('productos',array('crud'=>$crud,'edit'=>$edit),TRUE);
            $this->loadView($crud);
        }
        
        function movimientos($producto) {
        $crud = $this->crud_function('', '');
        $crud->unset_add()
                ->unset_edit()
                ->unset_delete()
                ->unset_export()
                ->unset_print()
                ->unset_read();
        $crud->order_by('id','DESC');
        $crud->display_as('productos_id', 'Producto')
                ->display_as('user_id', 'Usuario')
                ->display_as('motivo_movimiento', 'Descripción');
        $crud->columns('productos_id', 'motivo_movimiento', 'cantidad', 'fecha_carga', 'user_id', 'tipo');
        $crud->callback_column('cantidad', function($val, $row) {
            $badge = $row->tipo == -1 ? 'danger' : 'success';
            return '<span class="label label-' . $badge . '">' . $val . '</span>';
        });
        $crud->field_type('tipo', 'dropdown', array('-1' => 'Disminución', '1' => 'Incremento'));
        $crud->where('productos_id', $producto);
        $crud = $crud->render();
        $this->loadView($crud);
    }

    function paquetes(){
            $crud = $this->crud_function('','');        
            
            $crud->callback_field('productos',function($val,$row){
                return $this->load->view('_paquetesProductosForm',array('val'=>$val),TRUE);
            });
            $crud->callback_column('precio_venta',function($val,$row){
                return '$'.number_format($val,2,'.',',');
            });
            $crud->callback_before_delete(function($id){
                if($this->db->get_Where('ventas',array('paquetes_id'=>$id))->num_rows()>0){
                    return false;
                }
                return true;
            });
            $crud->where('sucursales_id',$this->user->sucursal);
            $crud->field_type('grupo','dropdown',array('1'=>'Servicio','2'=>'Producto'));
            $crud->display_as('id','Número de paquete')
                     ->display_as('precio_venta','Precio')
                    ->display_as('cantidad','Número de sesiones')
                     ->display_as('descripcion','Descripción');
            $crud->columns('id','nombre','descripcion','cantidad','precio_venta');
            $crud = $crud->render();            
            $this->loadView($crud);
        }
        
        function sesiones(){
            $this->as['sesiones'] = 'ventas';
            $crud = $this->crud_function('','');
            $crud->field_type('status','true_false',array('0'=>'Por cobrar','1'=>'Pagada'));
            $crud->field_type('paquetes_id','hidden','');
            if($crud->getParameters()=='list'){
                $crud->set_relation('clientes_id','clientes','{id}|{nombre}');
            }
            $crud->columns('id','j3eb7f57f.id','j3eb7f57f.nombre','fecha_venta','user_id','sucursales_id','totalizado','status');
            $crud->display_as('j3eb7f57f.id','Número de cliente')->display_as('j3eb7f57f.nombre','Nombre');
            $crud->display_as('user_id','Atendido por')
                 ->display_as('fecha_venta','Fecha de sesión')
                 ->display_as('sucursales_id','Sucursal')
                 ->display_as('id','Número de sesión')
                 ->display_as('total_venta','Total')
                 ->display_as('totalizado','Total');
            $crud->callback_column('j3eb7f57f.id',function($val,$row){return explode('|',$row->s3eb7f57f)[0];});
            $crud->callback_column('j3eb7f57f.nombre',function($val,$row){return explode('|',$row->s3eb7f57f)[1];});
            $crud->callback_column('totalizado',function($val,$row){
                return '$'.number_format($val,2,'.',',');
            });
            $crud->callback_before_delete(function($primary){
                $this->bd = get_instance()->bd;
                $venta = $this->db->get_Where('ventas',array('id'=>$primary));
                if($venta->num_rows()>0){
                    if(!empty($venta->row()->paquetes_id)){
                        $paquete = $venta->row()->paquetes_id;
                        $paquete = $this->db->get_where('clientes_paquetes',array('id'=>$paquete));
                        if($paquete->num_rows()>0){
                            $paquete = $paquete->row();
                            $this->db->update('clientes_paquetes',array('otorgados'=>$paquete->otorgados-1,'disponibles'=>$paquete->disponibles+1),array('id'=>$paquete->id));
                        }

                    }
                }
                $this->db->delete('ventas_detalles',array('ventas_id'=>$primary));
            });
            $crud->where('sucursales_id',$this->user->sucursal);
            $crud->unset_add()
                 ->unset_export()
                 ->unset_print()
                 ->unset_read();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function administrar_diagnosticos($cliente = '',$accion = "",$id = ""){            
            if(!is_numeric($cliente)){
                $this->as['administrar_diagnosticos'] = 'clientes';
                $crud = $this->crud_function('','');
                $crud->unset_add()
                     ->unset_edit()
                     ->unset_delete()
                     ->unset_print()
                     ->unset_export()
                     ->unset_read();                
                $crud->display_as('id','#Cliente');
                $crud->columns('id','nombre');
                $crud->callback_column('nombre',function($val,$row){
                    return '<a href="'.base_url('archivo/administrar_diagnosticos/'.$row->id).'">'.$val.'</a>';
                });
                $crud = $crud->render();
                $crud->title = 'Seleccione un cliente';
                $this->loadView($crud);
            }else{
                $this->as['administrar_diagnosticos'] = 'diagnosticos';
                $crud = $this->crud_function('','');                
                $crud->field_type('enfermedades','set',array(
                    'Cancer',
                    'Diabetes',
                    'Epilepsia',
                    'Varices',
                    'Lupus',
                    'Problemas Cardiacos',
                    'Problemas de tiroides',
                    'Problemas respiratorios',
                    'Desequilibrios hormonales',
                    'Alergias'
                ));

                $crud->field_type('como_rasura','enum',array(
                    'Con agua tibia y jabón',
                    'Con rasuradora eléctrica',
                ));
                $crud->field_type('agua','enum',array(
                    '2 Vasos',
                    '4 Vasos',
                    '6 Vasos',
                    '8 Vasos',
                    'Más de 8 vasos',
                ));
                $crud->field_type('bebidas_alcolicas','enum',array(
                    '1 a 3',
                    '4 a 5',
                    'Más de 5',
                ));
                $crud->order_by('id','DESC');
                $crud->display_as('id','Número de diagnóstico')
                     ->display_as('tratamiento_medico','Presenta tratamiento Médico (especifique)')
                     ->display_as('cirujia','Ha tenido cualquier tipo de cirugia en los ultimos meses (especifique)')
                     ->display_as('enfermedades','Tiene alguna de las siguientes enfermedades')
                     ->display_as('medicamento','Toma el medicamento (especifique)')
                     ->display_as('vitamina','Toma las vitaminas (especifique)')
                     ->display_as('fuma','¿Fuma?')
                     ->display_as('peeung_quimicos','Ha tenido peeung quimicos')
                     ->display_as('retyna','Usa retyna')
                     ->display_as('accutane_acne','Ha utilizado accutane para el acné')
                     ->display_as('dieta','¿Lleva continuamente una dieta?')
                     ->display_as('lentes_contacto','Usa lentes de contacto?')
                     ->display_as('temperatura_agua','¿A que temperatura usa el agua para lavarse?')
                     ->display_as('problema_cutis','ha tenido el problema especial de cutis (especifique)')
                     ->display_as('tratamiento_corporal','Ha tenido el tratamiento corpora (especifique)')
                     ->display_as('anticonceptivos','¿Toma anticonceptivos?')
                     ->display_as('embarazada','¿Esta embarazada o planificandolo?')
                     ->display_as('lactando','¿Esta lactando?')
                     ->display_as('como_rasura','¿Como se rasura?')
                     ->display_as('entierra_bellos_barba','¿Se le entierran los bellos en la barba?')
                     ->display_as('piel_grasosa','¿Experimenta la piel brillanteo grasosa durante el dia?')
                     ->display_as('acne','¿Tiene brotes de acné comederos o puntos negros?')
                     ->display_as('agua','Cantidad de agua que consume al día')
                     ->display_as('diureticos_laxantes','¿Toma diureticos o laxantes?')
                     ->display_as('bebidas_alcolicas','Bebidas alcolicas que usted consume semanalmente')
                     ->display_as('bloqueador','Utiliza bloqueador diario?')
                     ->display_as('user_id','Atendido por')
                    ->display_as('fecha','Fecha de diagnostico');
                $crud->columns('id','user_id','fecha');
                $crud->where('clientes_id',$cliente);
                if($crud->getParameters()!='list'){
                    $crud->field_type('clientes_id','hidden',$cliente)
                            ->field_type('user_id','hidden',$this->user->id)
                            ->field_type('fecha','hidden',date("Y-m-d"))
                            ->field_type('fuma','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('peeung_quimicos','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('retyna','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('accutane_acne','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('dieta','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('lentes_contacto','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('temperatura_agua','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('anticonceptivos','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('embarazada','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('entierra_bellos_barba','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('piel_grasosa','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('acne','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('diureticos_laxantes','true_false',array('0'=>'NO','1'=>'SI'))                            
                            ->field_type('bloqueador','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('lactando','true_false',array('0'=>'NO','1'=>'SI'));
                }
                $crud = $crud->render();
                $crud = $this->get_header($crud,$cliente);
                $crud->title = 'Diagnosticos';
                $crud->output = $this->load->view('diagnosticos',array('crud'=>$crud),TRUE,'expediente');
                $this->loadView($crud);
            }
        }
        
        function clientes_paquetes_admin(){ 
            $this->as['clientes_paquetes_admin'] = 'clientes_paquetes';
            $crud = $this->crud_function('','');
            $crud->set_relation('paquetes_id','paquetes','nombre'); 
            $crud->columns('id','clientes_id','user_id','fecha_venta','unidad_medida','precio','abono','totalizado','status','disponibles');
            $crud->where('clientes_paquetes.sucursales_id',$this->user->sucursal);
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
