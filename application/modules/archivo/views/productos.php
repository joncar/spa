<?= $crud->output ?>
<script>
    <?php if($edit): ?>
        var stockOriginal = parseFloat($("#field-stock").val());
        $("#field-addStock").on("change",function(){        
            var add = parseFloat($(this).val());
            if(isNaN(add)){
                $("#field-stock").val(stockOriginal);
            }else{
                $("#field-stock").val(stockOriginal+add);
            }
        });


        function calcular_aplicaciones(){
        	var aplicadas = parseInt($("#field-cantidad_aplicaciones").val());
    	    var realizadas = parseInt($("#field-aplicaciones_realizadas").val());
    	    var restantes = parseInt($("#field-aplicaciones_restantes").val());
    	    if(!isNaN(aplicadas) && !isNaN(realizadas) && !isNaN(restantes)){
    	    	restantes = aplicadas-realizadas;
    	    	$("#field-aplicaciones_restantes").val(restantes);
    	    }
        }
        $(document).on("change","#field-cantidad_aplicaciones",function(){
        	calcular_aplicaciones();
        });
        calcular_aplicaciones();
    <?php endif ?>
    $("#field-aplicaciones_restantes, #field-aplicaciones_realizadas").attr('readonly',true);
    var aplicaciones = $("#cantidad_aplicaciones_field_box,#aplicaciones_restantes_field_box,#aplicaciones_realizadas_field_box");
    $("#field-tipo").on("change",function(){
        if($(this).val()=='2'){
            aplicaciones.show();
        }else{
            aplicaciones.hide();
        }
    });
    $("#field-tipo").trigger('change');
</script>   