<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading1">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapseOne">
                    Reporte Diario
                </a>
            </h4>
        </div>
        <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
            <div class="panel-body">
                <?php $this->load->view('includes/template/dashboard') ?>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading3">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
                    Ventas por día
                </a>
            </h4>
        </div>
        <div id="collapse3" data-url="ventas_dia" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
            <div class="panel-body">
                <div class="row" style="margin:20px">
                    <div class="col-xs-12 col-sm-6">
                        <label for="fechaDesde">DESDE:</label>
                        <input type="text" class="form-control datepick fechaDesde" id="" value="<?= date("d-m-Y") ?>">
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <label for="fechaHasta">HASTA:</label>
                        <input type="text" class="form-control datepick fechaHasta" id="" value="<?= date("d-m-Y") ?>">
                    </div>
                </div>
                <?= $reportes[3]->output ?>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Reporte de clientes
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <?= $reportes[0]->output ?>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Reporte de productos
                </a>
            </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
                <?= $reportes[1]->output ?>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Reporte de inventario
                </a>
            </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
                <?= $reportes[2]->output ?>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading4">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                    Resumen de pagos recibidos
                </a>
            </h4>
        </div>
        <div id="collapse4" data-url="reporte_pagos_recibidos" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
            <div class="panel-body">
                <div class="row" style="margin:20px">
                    <div class="col-xs-12 col-sm-6">
                        <label for="fechaDesde">DESDE:</label>
                        <input type="text" class="form-control datepick fechaDesde" id="" value="<?= date("d-m-Y") ?>">
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <label for="fechaHasta">HASTA:</label>
                        <input type="text" class="form-control datepick fechaHasta" id="" value="<?= date("d-m-Y") ?>">
                    </div>
                </div>
                <?= $reportes[4]->output ?>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('predesign/datepicker'); ?>
<script>
    $('.datepick').datepicker({
        format:    "dd-mm-yyyy",
        autoclose: true,
        /*startDate: new Date(),*/
        weekStart: 1
    });

    $("#collapse3 #filtering_form").append('<input type="hidden" id="desde" name="desde">');
    $("#collapse3 #filtering_form").append('<input type="hidden" id="hasta" name="hasta">');
    
    $("#collapse4 #filtering_form").append('<input type="hidden" id="desde" name="desde">');
    $("#collapse4 #filtering_form").append('<input type="hidden" id="hasta" name="hasta">');
    
    $(".datepick").on("change",function(){
        var d = $(this).parents('.panel-collapse').find(".fechaDesde").val();
        var h = $(this).parents('.panel-collapse').find(".fechaHasta").val();
        var url = $(this).parents('.panel-collapse').data('url');
        $(this).parents('.panel-collapse').find("#filtering_form #desde").val(d);
        $(this).parents('.panel-collapse').find("#filtering_form #hasta").val(h);
        $(this).parents('.panel-collapse').find("#filtering_form .ajax_refresh_and_loading").trigger('click');

        $(this).parents('.panel-collapse').find(".buttons-csv").attr('href','<?= base_url('reportes/rep') ?>/'+url+'/export?desde='+d+'&hasta='+h);
    });
</script>