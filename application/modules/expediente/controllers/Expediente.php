<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Expediente extends Panel{
        function __construct() {
            parent::__construct();
        }

        function get_header($crud,$cliente){
            $this->as[$this->router->fetch_method()] = 'clientes';
            $crud->header = $this->crud_function('','');
            $crud->header->set_subject('Cliente');
            $crud->header->where('clientes.id',$cliente);
            $crud->header->set_theme('header_data');
            $crud->header->set_url('archivo/clientes');
            $crud->header->columns('id','nombre','telefono','email','direccion','cumpleaños','referencia','total_sesiones');
            $crud->header->display_as('id','#Cliente');
            $crud->header->callback_column('total_sesiones',function($val,$row){
                return (string)$this->db->get_where('ventas',array('clientes_id'=>$row->id))->num_rows();
            });
            $crud->header = $crud->header->render(1)->output;
            return $crud;
        }

        function expedientes($cliente = '',$accion = "",$id = ""){            
            if(!is_numeric($cliente)){
                $this->as['expedientes'] = 'clientes';
                $crud = $this->crud_function('','');
                $crud->unset_add()
                     ->unset_edit()
                     ->unset_delete()
                     ->unset_print()
                     ->unset_export()
                     ->unset_read();                
                $crud->display_as('id','#Cliente');
                $crud->columns('id','nombre','total_sesiones');
                $crud->callback_column('total_sesiones',function($val,$row){
                    return (string)$this->db->get_where('ventas',array('clientes_id'=>$row->id))->num_rows();
                });
                $crud->callback_column('nombre',function($val,$row){
                    return '<a href="'.base_url('expediente/expedientes/'.$row->id).'">'.$val.'</a>';
                });
                $crud = $crud->render();
                $crud->title = 'Seleccione un cliente';
                $this->loadView($crud);
            }else{
                $this->as['expedientes'] = 'ventas';
                $crud = $this->crud_function('','');
                $crud->unset_add()
                     ->unset_edit()
                     ->unset_print()
                     ->unset_export()
                     ->unset_delete();
                $crud->order_by('id','DESC');
                $crud->display_as('sucursales_id','Sucursal')
                         ->display_as('clientes_id','Datos del cliente')
                         ->display_as('id','Número de sesión')
                         ->display_as('fecha_venta','Fecha de sesión')
                        ->display_as('total_venta','Total')
                        ->display_as('user_id','Atendido por');
                $crud->columns('id','fecha_venta','user_id','sucursales_id','total_venta');
                $crud->callback_column('total_venta',function($val,$row){
                    return '$'.number_format($val,2,'.',',');
                });
                $crud->where('clientes_id',$cliente);                     
                $crud->where('ventas.sucursales_id',$this->user->sucursal);
                $crud->field_type('status','true_false',array('0'=>'Pendiente','1'=>'Pagado'));
                $crud->callback_column('id',function($val,$row){
                    return '<a href="'.base_url('procesos/ventas_cobrar/'.$row->clientes_id.'/leer/'.$val).'">'.$val.'</a>';
                });
                if($crud->getParameters()=='list'){
                    $crud->unset_read();
                }
                $crud = $crud->render();
                $crud = $this->get_header($crud,$cliente);
                $crud->title = 'Expedientes';
                $crud->output = $this->load->view('list',array('crud'=>$crud),TRUE);
                if($accion=="read"){
                    $ventas = $this->db->get_where('ventas_detalles',array('ventas_id'=>$id));
                    $crud->output = $this->load->view('tratamientos',array('venta'=>$ventas,'crud'=>$crud,'cliente'=>$cliente),TRUE);
                }
                $this->loadView($crud);
            }
        }

        function diagnosticos($cliente = '',$accion = "",$id = ""){            
            if(!is_numeric($cliente)){
                $this->as['diagnosticos'] = 'clientes';
                $crud = $this->crud_function('','');
                $crud->unset_add()
                     ->unset_edit()
                     ->unset_delete()
                     ->unset_print()
                     ->unset_export()
                     ->unset_read();                
                $crud->display_as('id','#Cliente');
                $crud->columns('id','nombre');
                $crud->callback_column('nombre',function($val,$row){
                    return '<a href="'.base_url('expediente/diagnosticos/'.$row->id).'">'.$val.'</a>';
                });
                $crud = $crud->render();
                $crud->title = 'Seleccione un cliente';
                $this->loadView($crud);
            }else{
                $crud = $this->crud_function('','');
                if($crud->getParameters()=='list'){
                    $crud->unset_print()
                         ->unset_read()
                         ->unset_edit()
                         ->unset_export()
                         ->unset_delete();
                }
                $crud->field_type('enfermedades','set',array(
                    'Cancer',
                    'Diabetes',
                    'Epilepsia',
                    'Varices',
                    'Lupus',
                    'Problemas Cardiacos',
                    'Problemas de tiroides',
                    'Problemas respiratorios',
                    'Desequilibrios hormonales',
                    'Alergias'
                ));

                $crud->field_type('como_rasura','enum',array(
                    'Con agua tibia y jabón',
                    'Con rasuradora eléctrica',
                ));
                $crud->field_type('agua','enum',array(
                    '2 Vasos',
                    '4 Vasos',
                    '6 Vasos',
                    '8 Vasos',
                    'Más de 8 vasos',
                ));
                $crud->field_type('bebidas_alcolicas','enum',array(
                    '1 a 3',
                    '4 a 5',
                    'Más de 5',
                ));
                $crud->order_by('id','DESC');
                $crud->display_as('id','Número de diagnóstico')
                     ->display_as('tratamiento_medico','Presenta tratamiento Médico (especifique)')
                     ->display_as('cirujia','Ha tenido cualquier tipo de cirugia en los ultimos meses (especifique)')
                     ->display_as('enfermedades','Tiene alguna de las siguientes enfermedades')
                     ->display_as('medicamento','Toma el medicamento (especifique)')
                     ->display_as('vitamina','Toma las vitaminas (especifique)')
                     ->display_as('fuma','¿Fuma?')
                     ->display_as('peeung_quimicos','Ha tenido peeung quimicos')
                     ->display_as('retyna','Usa retyna')
                     ->display_as('accutane_acne','Ha utilizado accutane para el acné')
                     ->display_as('dieta','¿Lleva continuamente una dieta?')
                     ->display_as('lentes_contacto','Usa lentes de contacto?')
                     ->display_as('temperatura_agua','¿A que temperatura usa el agua para lavarse?')
                     ->display_as('problema_cutis','ha tenido el problema especial de cutis (especifique)')
                     ->display_as('tratamiento_corporal','Ha tenido el tratamiento corpora (especifique)')
                     ->display_as('anticonceptivos','¿Toma anticonceptivos?')
                     ->display_as('embarazada','¿Esta embarazada o planificandolo?')
                     ->display_as('lactando','¿Esta lactando?')
                     ->display_as('como_rasura','¿Como se rasura?')
                     ->display_as('entierra_bellos_barba','¿Se le entierran los bellos en la barba?')
                     ->display_as('piel_grasosa','¿Experimenta la piel brillanteo grasosa durante el dia?')
                     ->display_as('acne','¿Tiene brotes de acné comederos o puntos negros?')
                     ->display_as('agua','Cantidad de agua que consume al día')
                     ->display_as('diureticos_laxantes','¿Toma diureticos o laxantes?')
                     ->display_as('bebidas_alcolicas','Bebidas alcolicas que usted consume semanalmente')
                     ->display_as('bloqueador','Utiliza bloqueador diario?')
                     ->display_as('user_id','Atendido por')
                    ->display_as('fecha','Fecha de diagnostico');
                $crud->columns('id','user_id','fecha');
                $crud->where('clientes_id',$cliente);
                if($crud->getParameters()!='list'){
                    $crud->field_type('clientes_id','hidden',$cliente)
                            ->field_type('user_id','hidden',$this->user->id)
                            ->field_type('fecha','hidden',date("Y-m-d"))
                            ->field_type('fuma','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('peeung_quimicos','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('retyna','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('accutane_acne','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('dieta','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('lentes_contacto','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('temperatura_agua','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('anticonceptivos','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('embarazada','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('entierra_bellos_barba','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('piel_grasosa','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('acne','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('diureticos_laxantes','true_false',array('0'=>'NO','1'=>'SI'))                            
                            ->field_type('bloqueador','true_false',array('0'=>'NO','1'=>'SI'))
                            ->field_type('lactando','true_false',array('0'=>'NO','1'=>'SI'));
                }
                $crud->callback_column('id',function($val,$row){
                    return '<a href="'.base_url('reportes/rep/visualizarReporte/43/html/diagnosticos_id/'.$val).'" target="_new">'.$val.'</a>';
                });
                $crud = $crud->render();
                $crud = $this->get_header($crud,$cliente);
                $crud->title = 'Diagnosticos';
                $crud->output = $this->load->view('diagnosticos',array('crud'=>$crud),TRUE);
                $this->loadView($crud);
            }
        }
    }
?>
 