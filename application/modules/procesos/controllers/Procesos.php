<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Procesos extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function get_venta_activa($cliente){
            $venta = $this->db->get_where('ventas',array('clientes_id'=>$cliente,'status'=>0));
            $cliente = $this->db->get_where('clientes',array('id'=>$cliente));
            if($cliente->num_rows()>0){
                $cliente = $cliente->row()->id;
                if($venta->num_rows()==0){
                    $this->db->insert('ventas',array('clientes_id'=>$cliente,'fecha_venta'=>date("Y-m-d H:i:s"),'status'=>0,'total_venta'=>0,'user_id'=>$this->user->id));
                    $venta = $this->db->insert_id();
                }
                else{
                    $venta = $venta->row()->id;
                }
            }else{
                redirect('archivo/clientes');
                die();
            }
            return $venta;
        }

        function get_header($crud,$cliente,$id = ""){
            if(!is_numeric($id)){
                $this->as[$this->router->fetch_method()] = 'clientes';
                $crud->header = $this->crud_function('','');
                $crud->header->set_subject('Cliente');
                $crud->header->where('clientes.id',$cliente);
                $crud->header->set_theme('header_data');
                $crud->header->set_url('archivo/clientes');           
                $crud->header->display_as('id','Número de cliente');
                $crud->header->columns('id','nombre');
                $crud->header = $crud->header->render(1)->output;
                return $crud;
            }
            else{
                $this->as[$this->router->fetch_method()] = 'ventas';
                $crud->header = $this->crud_function('','');
                $crud->header->set_subject('Cliente');
                $crud->header->where('clientes_id',$cliente);
                $crud->header->set_theme('header_data');
                $crud->header->set_url('archivo/clientes');
                $crud->header->set_relation('clientes_id','clientes','{id}|{nombre}');
                $crud->header->columns('#Cliente','Nombre','user_id');
                $crud->header->callback_column('#Cliente',function($val,$row){
                    return explode('|',$row->s3eb7f57f)[0];
                });
                $crud->header->callback_column('Nombre',function($val,$row){
                    return explode('|',$row->s3eb7f57f)[1];
                });
                $crud->header->display_as('clientes_id','Nombre')
                             ->display_as('fecha_venta','Fecha')
                             ->display_as('user_id','Atendido por')
                             ->display_as('sucursales_id','Sucursal');
                $crud->header->display_as('#Cliente','Número de cliente');
                $crud->header = $crud->header->render(1)->output;
                return $crud;
            }
        }
        function cargar_productos($cliente = '',$accion = "",$subaccion = ''){

            $this->as['cargar_productos'] = 'ventas';
            $crud = $this->crud_function('','');
            if($accion!='add'){
                $crud->unset_add()
                     ->unset_edit()
                     ->unset_print()
                     ->unset_read()
                     ->unset_delete()
                     ->unset_export();
                $crud->where('status',0);
                $crud->order_by('id','DESC');
                $crud->field_type('status','true_false',array(0=>'Por cobrar',1=>'Pagada'));
                $crud = $crud->render();            
                $crud = $this->get_header($crud,$cliente);
                $crud->output = $this->load->view('tratamientos',array('cliente'=>$cliente,'crud'=>$crud),TRUE);
                $crud->title = 'Sesiones';
            }else{
                $crud->required_fields('status');
                if($accion=='add' && $subaccion!='update'){
                    $crud->set_rules('status','Status','required|callback_validate_productos');
                }
                $crud->callback_after_insert(function($post,$primary){
                    $this->db = get_instance()->db;
                    
                    $pos = $_POST['detalles']['productos_id'];
                    $precios = $_POST['detalles']['precio'];
                    $unidades = $_POST['detalles']['unidad_medida'];
                    $tratamientos = $_POST['detalles']['tratamiento'];
                    $muestras = $_POST['detalles']['muestra'];
                    $cantidades = $_POST['detalles']['cantidad'];
                    $total = 0;
                    foreach($pos as $n=>$v){
                        $v = array(
                            'productos_id'=>$pos[$n],
                            'precio'=>$precios[$n],
                            'unidad_medida'=>$unidades[$n],
                            'tratamiento'=>$tratamientos[$n],
                            'muestra'=>$muestras[$n],
                            'cantidad'=>$cantidades[$n]

                        );





                        $v['precio'] = str_replace('$','',$v['precio']);
                        $v['precio'] = str_replace(',','',$v['precio']);
                        $v['precio'] = str_replace('.','.',$v['precio']);                        
                        $v['total'] = $v['precio']*$v['cantidad'];
                        $total+=$v['total'];
                        $v['ventas_id'] = $primary;
                        get_instance()->db->insert('ventas_detalles',$v);                        
                        $stock = $this->db->get_where('productos',array('id'=>$v['productos_id'],'sucursales_id'=>$this->user->sucursal));
                        $grupo = $stock->num_rows()==0?1:$stock->row()->grupo;
                        if($grupo==2){
                            if($stock->num_rows()>0){
                                $stock = $stock->row();
                                
                                if($v['tratamiento']==1){
                                    //Verificar aplicaciones                                    
                                    $aplicaciones = $stock->aplicaciones_realizadas+$v['unidad_medida'];
                                    $max_aplicaciones = $stock->cantidad_aplicaciones;
                                    if($aplicaciones>=$max_aplicaciones){                                        
                                        $usados = $aplicaciones/$max_aplicaciones;
                                        $usados = floor($usados);
                                        $stock->stock-= $usados;
                                        //Sacar aplicaciones del nuevo
                                        $usando = $aplicaciones-($usados*$max_aplicaciones);
                                        $aplicaciones = $usando;                                        
                                    }
                                    $restantes = $max_aplicaciones-$aplicaciones;
                                    $this->db->update('productos',array('stock'=>$stock->stock,'aplicaciones_restantes'=>$restantes,'aplicaciones_realizadas'=>$aplicaciones),array('id'=>$stock->id));
                                }else{
                                    $stock->stock-= $v['cantidad'];
                                    $this->db->update('productos',array('stock'=>$stock->stock),array('id'=>$stock->id));
                                }
                                                                                                                                
                                $this->db->insert('movimientos',array(
                                    'productos_id'=>$v['productos_id'],
                                    'cantidad'=>$v['cantidad'],
                                    'precio_venta'=>$v['precio'],
                                    'precio_compra'=>0,
                                    'motivo_movimiento'=>'Venta unitaria',
                                    'fecha'=>date("Y-m-d"),
                                    'fecha_carga'=>date("Y-m-d"),
                                    'user_id'=>$this->user->id,
                                    'tipo'=>-1,
                                    'sucursales_id'=>$post['sucursales_id']
                                ));
                            }
                        }
                    }
                    $this->db->update('ventas',array('total_venta'=>$total),array('id'=>$primary));                                        
                    //Descontar paquete
                    if(!empty($post['paquetes_id'])){                        
                        $paquete = $this->db->get_where('clientes_paquetes',array('id'=>$post['paquetes_id']))->row();
                        $disponibles = $paquete->disponibles-1;
                        $otorgados = $paquete->otorgados+1;
                        $status = $disponibles<=0?0:1;
                        $this->db->update('clientes_paquetes',array('otorgados'=>$otorgados,'ultima_sesion'=>date("Y-m-d"),'disponibles'=>$disponibles,'status'=>$status),array('id'=>$post['paquetes_id']));
                        $this->db->update('ventas',array('status'=>1),array('id'=>$primary));                                        
                    }
                });
                $crud = $crud->render();
            }
            $this->loadView($crud);
        }

        function validate_productos(){
            if(                
                empty($_POST['detalles']['productos_id'][0]) &&
                empty($_POST['detalles']['productos_id'][0]) &&
                empty($_POST['detalles']['productos_id'][0])
            ){
                $this->form_validation->set_message('validate_productos','Por favor verifique los productos');
                return false;
            }
        }

        function ventas(){
            $this->as['ventas'] = 'clientes';
            $crud = $this->crud_function('','');
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read();
            $crud->display_as('id','#Cliente');
            $crud->columns('id','nombre');                
            $crud->callback_column('nombre',function($val,$row){
                return '<a href="'.base_url('procesos/cargar_productos/'.$row->id).'">'.$val.'</a>';
            });
            $crud = $crud->render();
            $crud->title = 'Seleccione un cliente';
            $this->loadView($crud);
        }

        function ventas_cobrar($cliente = '',$accion = "",$id = ""){
            if(!is_numeric($cliente)){
                $this->as['ventas_cobrar'] = 'clientes';
                $crud = $this->crud_function('','');
                $this->cliente = $cliente;
                $crud->unset_add()
                     ->unset_edit()
                     ->unset_delete()
                     ->unset_print()
                     ->unset_export()
                     ->unset_read();
                $crud->display_as('id','#Cliente');
                $crud->columns('id','nombre');                
                $crud->callback_column('nombre',function($val,$row){
                    return '<a href="'.base_url('procesos/ventas_cobrar/'.$row->id).'">'.$val.'</a>';
                });
                $crud = $crud->render();
                $crud->title = 'Seleccione un cliente';
                $this->loadView($crud);
            }else{
                $this->as['ventas_cobrar'] = 'ventas';
                $crud = $this->crud_function('','');
                $crud->unset_add()
                     ->unset_edit()
                     ->unset_print()
                     ->unset_export()
                     ->unset_delete();
                if($crud->getParameters()=='list'){
                    $crud->unset_read();
                }
                $crud->order_by('id','DESC');                     
                $crud->where('clientes_id',$cliente)
                     ->where('ventas.sucursales_id',$this->user->sucursal)
                     ->where('ventas.status <',1);
                $crud->field_type('status','true_false',array('0'=>'Por cobrar','1'=>'Pagada'));
                $crud->display_as('clientes_id','Nombre')
                         ->display_as('user_id','Atendido por')
                        ->display_as('status','Estatus')
                         ->display_as('fecha_venta','Fecha de sesión')
                        ->display_as('id','#Sesión')
                         ->display_as('total_venta','Total');
                $crud->columns('id','clientes_id','fecha_venta','observaciones','total_venta','user_id','status');
                $crud->callback_column('id',function($val,$row){
                    return '<a href="'.base_url('procesos/ventas_cobrar/'.$row->clientes_id.'/read/'.$val).'">'.$val.'</a>';
                });
                $crud->callback_column('total_venta',function($val,$row){
                    return '$'.number_format($val,2,'.',',');
                });
                $crud = $crud->render();
                $crud = $this->get_header($crud,$cliente,$id);
                $crud->paquetes = $this->getPaquetesPorCobrar($cliente);
                $crud->output = $this->load->view('_ventas_cobrar',array('crud'=>$crud),TRUE);
                $crud->title = 'Sesiones por cobrar';
                if($accion=="read" || $accion == 'leer'){
                    $vent = $this->db->get_where('ventas',array('id'=>$id));
                    $vent = $vent->row();
                    $vent->read = $accion=='read'?false:true;
                    $crud->title = $accion!='read'?'Detalle de sesiones':$crud->title;
                    $ventas = $this->db->get_where('ventas_detalles',array('ventas_id'=>$id));
                    $crud->output = $this->load->view('tratamientos',array('vent'=>$vent,'venta'=>$ventas,'crud'=>$crud,'cliente'=>$cliente),TRUE);
                }
                $this->loadView($crud);
            }
        }
        
        function getPaquetesPorCobrar($cliente){
            $this->as['ventas_cobrar'] = 'clientes_paquetes';
            $this->as['getPaquetesPorCobrar'] = 'clientes_paquetes';
            $crud = $this->crud_function("","");
            $crud->display_as('clientes_id','Nombre')
                         ->display_as('user_id','Atendido por')
                         ->display_as('fecha_venta','Fecha de asignación')
                        ->display_as('id','#Asignación')
                         ->display_as('total_paquete','Total');
            $crud->columns('id','clientes_id','fecha_venta','observaciones','total_paquete','abono','user_id');
            if($crud->getParameters()!='read'){
                $crud->unset_add()
                     ->unset_edit()
                     ->unset_read()
                     ->unset_print()
                     ->unset_export()
                     ->unset_delete();
            }
            $crud->callback_column('id',function($val,$row){
                return '<a href="'.base_url('procesos/paquetes_asignados/'.$row->clientes_id.'/read/'.$val).'">'.$val.'</a>';
            });
            $crud->where('clientes_id = '.$cliente.' AND abono<totalizado AND j20127754.sucursales_id = '.$this->user->sucursal,'ESCAPE',FALSE); 
            $crud->set_url('procesos/getPaquetesPorCobrar/'.$cliente.'/');
            $crud = $crud->render();
            return $crud->output;
        }
        
        function clientes_paquetes($cliente = '',$accion = "",$id = ""){
            if(!is_numeric($cliente)){
                $this->as['clientes_paquetes'] = 'clientes';
                $crud = $this->crud_function('','');
                $this->cliente = $cliente;
                $crud->unset_add()
                     ->unset_edit()
                     ->unset_delete()
                     ->unset_print()
                     ->unset_export()
                     ->unset_read();
                $crud->display_as('id','#Cliente');
                $crud->columns('id','nombre');                
                $crud->callback_column('nombre',function($val,$row){
                    return '<a href="'.base_url('procesos/clientes_paquetes/'.$row->id).'">'.$val.'</a>';
                });
                $crud = $crud->render();
                $crud->title = 'Seleccione un cliente';
                $this->loadView($crud);
            }else{                
                if($accion!='add'){                
                    $crud = $this->crud_function('','');
                    $crud->unset_add()
                         ->unset_edit()
                         ->unset_print()
                         ->unset_read()
                         ->unset_delete()
                         ->unset_export();
                    $crud->where('status',0);
                    $crud->order_by('id','DESC');
                    $crud->field_type('status','true_false',array(0=>'Por cobrar',1=>'Pagada'));
                    $crud = $crud->render();            
                    $crud = $this->get_header($crud,$cliente);
                    $crud->output = $this->load->view('paquetes',array('cliente'=>$cliente,'crud'=>$crud),TRUE);
                    $crud->title = 'Asignar paquetes';
                    $this->loadView($crud);
                }else{
                    $crud = $this->crud_function('','');
                    if($crud->getParameters()=='edit'){
                        $crud->required_fields('abono');
                    }
                    $crud->callback_before_insert(function($post){
                        $post['precio'] = str_replace(',','',$post['precio']);
                        $post['precio'] = str_replace('$','',$post['precio']);
                        return $post;
                    });
                    $crud->callback_before_update(function($post,$primary){
                        $post['abono'] = str_replace(',','',$post['abono']);
                        $post['abono'] = str_replace('$','',$post['abono']);
                        $post['totalizado'] = str_replace(',','',$post['totalizado']);
                        $post['totalizado'] = str_replace('$','',$post['totalizado']);

                        $abonado = get_instance()->db->get_where('clientes_paquetes',array('id'=>$primary))->row()->abono;
                        get_instance()->db->insert('clientes_paquetes_pagos',array(
                            'clientes_paquetes_id'=>$primary,
                            'fecha'=>date("Y-m-d H:i:s"),
                            'importe'=>$post['abono'],
                            'forma_pago'=>$post['forma_pago']
                        ));
                        $post['abono']+=$abonado;
                        return $post;
                    });
                    $crud = $crud->render();            
                    $this->loadView($crud);
                }
            }
        }
        
        function paquetes_asignados($cliente = '',$accion = "",$id = ""){
            if(!is_numeric($cliente)){
                $this->as['paquetes_asignados'] = 'clientes';
                $crud = $this->crud_function('','');
                $this->cliente = $cliente;
                $crud->unset_add()
                     ->unset_edit()
                     ->unset_delete()
                     ->unset_print()
                     ->unset_export()
                     ->unset_read();
                $crud->display_as('id','#Cliente');
                $crud->columns('id','nombre');                
                $crud->callback_column('nombre',function($val,$row){
                    return '<a href="'.base_url('procesos/paquetes_asignados/'.$row->id).'">'.$val.'</a>';
                });
                $crud = $crud->render();
                $crud->title = 'Seleccione un cliente';
                $this->loadView($crud);
            }else{                
                $this->as['paquetes_asignados'] = 'clientes_paquetes';
                $crud = $this->crud_function('','');
                    $crud->unset_add()
                         ->unset_edit()
                         ->unset_print()
                         ->unset_delete()
                         ->unset_export();                    
                   if($crud->getParameters()=='list'){
                        $crud->unset_read();
                    }
                    $crud->where('clientes_id',$cliente)
                         ->where('j20127754.sucursales_id',$this->user->sucursal);
                    $crud->order_by('id','DESC');
                    $crud->field_type('status','true_false',array(0=>'Utilizado',1=>'Activo'));
                    $crud->set_relation('paquetes_id','paquetes','{nombre}|{descripcion}');
                    $crud->columns('id','js20127754.nombre','j20127754.descripcion','otorgados','disponibles','ultima_sesion','status');                                        
                    $crud->callback_column('js20127754.nombre',function($val,$row){
                        return explode('|',$row->s20127754)[0];
                    });
                    $crud->callback_column('j20127754.descripcion',function($val,$row){
                        return explode('|',$row->s20127754)[1];
                    });
                    $crud->callback_column('id',function($val,$row){
                        return '<a href="'.base_url('procesos/paquetes_asignados/'.$row->clientes_id.'/read/'.$val).'">'.$val.'</a>';
                    });
                    $crud->display_as('id','Número de asignación')->display_as('js20127754.nombre','Nombre')->display_as('j20127754.descripcion','Descripción')->display_as('otorgados','Sesiones otorgadas');
                    $crud = $crud->render();            
                    $crud = $this->get_header($crud,$cliente);
                    $crud->title = 'Paquetes asignados';
                    if($accion=="read" || $accion == 'leer'){
                        $vent = $this->db->get_where('clientes_paquetes',array('id'=>$id));
                        $vent = $vent->row();
                        $vent->read = $accion=='read'?false:true;
                        $crud->title = $accion!='read'?'Detalle de sesiones':$crud->title;
                        $crud->output = $this->load->view('paquetes',array('vent'=>$vent,'crud'=>$crud,'cliente'=>$cliente),TRUE);
                    }else{
                        $crud->output = $this->load->view('paquetes_asignados',array('cliente'=>$cliente,'crud'=>$crud),TRUE);
                    }
                    $this->loadView($crud);
            }
        }
    }
?>
