<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Productos extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function consultar_precios($x = ""){
            $this->as['consultar_precios'] = 'productos';
            $crud = $this->crud_function('',''); 
            $crud->field_type('grupo','dropdown',array('1'=>'Servicio','2'=>'Producto'))
                     ->field_type('user_id','hidden',$this->user->id)
                     ->field_type('cantidad_aplicaciones','string',1)
                     ->field_type('aplicaciones_realizadas','string',0)
                     ->field_type('tipo','dropdown',array('1'=>'Venta','2'=>'Uso'))
                     ->field_type('actualizacion','hidden',date("Y-m-d"));                        
            $crud->callback_column('stock',function($val,$row){                
                if($row->grupo=='Servicio'){
                    return 'N/A';
                }
                return $val;
            });            
            $crud->callback_column('precio_venta',function($val,$row){
                    return '$'.number_format($val,2,'.',',');
            });
            $crud->unset_searchs('actualizacion');
            $crud->display_as('codigo','Código')
                     ->display_as('stock','Stock actual')
                     ->display_as('actualizacion','Última modificación')
                     ->display_as('sucursales_id','Sucursal');
            $crud->columns('codigo','nombre','unidad_medida','precio_venta','grupo','min_stock','stock','sucursales_id','actualizacion');
            $crud->unset_add()->unset_edit()->unset_delete();
            $crud = $crud->render();             
            $this->loadView($crud);
        }

        function consultar_paquetes(){
            $this->as['consultar_paquetes'] = 'paquetes';
            $crud = $this->crud_function('','');
            $crud->callback_column('precio_venta',function($val,$row){
                return '$'.number_format($val,2,'.',',');
            });
            $crud->field_type('grupo','dropdown',array('1'=>'Servicio','2'=>'Producto'));
            $crud->display_as('id','Número de paquete')
                     ->display_as('precio_venta','Precio')
                    ->display_as('cantidad','Número de sesiones')
                     ->display_as('descripcion','Descripción');
            $crud->columns('id','nombre','descripcion','cantidad','precio_venta');
            $crud->unset_add()->unset_edit()->unset_delete();
            $crud = $crud->render();            
            $this->loadView($crud);
        }
    }
?>
