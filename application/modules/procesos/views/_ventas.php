<div id="mensaje"></div>
<?= $crud->output ?>
<script type="text/javascript">
	function cerrarVenta(id){
		$("#mensaje").removeClass('alert alert-success').html('');
		if(confirm("¿Esta seguro que desea cerrar esta venta?, esta acción no puede reversarse")){
			$.post('<?= base_url('procesos/ventas/cerrar/') ?>/'+id,{},function(data){
				$(".ajax_refresh_and_loading").trigger('click');
				$("#mensaje").addClass('alert alert-success').html("La venta se ha cerrado con éxito. <a href='<?= base_url() ?>reportes/rep/verReportes/36/html/ventas_id/"+id+"'>Imprimir Ticket</a>");
			});
		}
	}
</script>