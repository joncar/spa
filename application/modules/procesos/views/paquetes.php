<div><?= $crud->header ?></div>
<div id="msj"></div>
<div class="panel panel-default">
    <div class="panel-heading">
            <h1 class="panel-title">Añadir paquetes</h1>
    </div>
    <div class="panel-body">
    		<?php if(empty($vent)): ?>
            <?php $this->load->view('_formAddPaquetes',array('status'=>1),FALSE); ?>
        	<?php else: ?>
    		<?php $this->load->view('_formReadPaquetes',array('status'=>1),FALSE); ?>
        	<?php endif ?>
    </div>
</div>