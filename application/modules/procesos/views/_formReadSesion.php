<?php $total = 0; ?>
<form onsubmit="return save(this)">
    <div class="row" style="margin: 30px">
        <div class="col-xs-12 col-sm-4"><b>Fecha de la sesión:</b> <?= date("d/m/Y",strtotime($vent->fecha_venta)) ?></div>
        <div class="col-xs-12 col-sm-4"><b>Atendido por:</b> <?= $this->db->get_where('user',array('id'=>$vent->user_id))->row()->nombre ?></div>
        <div class="col-xs-12 col-sm-4"><b>Sucursal:</b> <?= $this->db->get_where('sucursales',array('id'=>$vent->sucursales_id))->row()->nombre ?></div>
    </div>
    <table class="table table-sorted">
        <tr>
            <th>Descripción</th>
            <th>Cant.</th>
            <th>Uni. Medida</th>
            <th>Precio</th>
        </tr>	
        <?php foreach($this->db->get_where('ventas_detalles',array('ventas_id'=>$venta->row()->ventas_id,'muestra'=>0,'tratamiento'=>0))->result() as $v): ?>
            <?php if(!empty($v->productos_id)): ?>
            <tr>
                <td><?= $this->db->get_where('productos', array('id' => $v->productos_id))->row()->nombre ?>(Comprado)</td>
                <td><?= $v->cantidad ?></td>
                <td><?= $v->unidad_medida ?></td>
                <td>$<?= number_format($v->precio, 2, '.', ',') ?></td>
                <?php $total+= $v->precio * $v->cantidad; ?>
            </tr>
            <?php endif ?>
        <?php endforeach ?>
        <?php foreach($this->db->get_where('ventas_detalles',array('ventas_id'=>$venta->row()->ventas_id,'muestra'=>1))->result() as $v): ?>
            <?php if(!empty($v->productos_id)): ?>
            <tr>
                <td><?= $this->db->get_where('productos', array('id' => $v->productos_id))->row()->nombre ?>(Muestra)</td>
                <td><?= $v->cantidad ?></td>
                <td><?= $v->unidad_medida ?></td>
                <td>$<?= number_format($v->precio, 2, '.', ',') ?></td>
                <?php $total+= $v->precio * $v->cantidad; ?>
            </tr>
            <?php endif ?>
        <?php endforeach ?>
        
        <?php foreach($this->db->get_where('ventas_detalles',array('ventas_id'=>$venta->row()->ventas_id,'tratamiento'=>1))->result() as $v): ?>            
            <?php if(!empty($v->productos_id)): ?>
            <tr>
                <td><?= $this->db->get_where('productos', array('id' => $v->productos_id))->row()->nombre ?>(Utilizado)</td>
                <td><?= $v->cantidad ?></td>
                <td><?= $v->unidad_medida ?></td>
                <td>$<?= number_format($v->precio, 2, '.', ',') ?></td>
                <?php $total+= $v->precio * $v->cantidad; ?>
            </tr>
            <?php endif ?>
        <?php endforeach ?>
        
        <?php if($vent->status==0): ?>
        <tr>
            <th colspan="3">Total Venta</th>
            <th>$<?= number_format($total, 2, '.', ',') ?></th>
        </tr>
        <tr>
            <th colspan="3">Descuento</th>
            <th><input type="text" id="descuento" value="" placeholder="0">%</th>
        </tr>
        <tr>
            <th colspan="3">Total a pagar</th>
            <th>$<input type="text" id="totalizado" value="<?= $total ?>" readonly></th>
        </tr>
        <tr>
            <th colspan="3">Forma de pago</th>
            <th>
                <select id="forma_pago" class="form-control" data-placeholder='Seleccione una forma de pago'>
                    <option>Efectivo</option>
                    <option>Transferencia</option>
                    <option>Cheque</option>
                    <option>Tarjeta de Debito</option>
                    <option>Tarjeta de Crédito</option>
                </select>
            </th>
        </tr>
        <?php else: ?>
            <tr>
            <th colspan="3">Total Venta</th>
            <th><?= '$'.number_format($total,2) ?></th>
        </tr>
        <tr>
            <th colspan="3">Descuento</th>
            <th><?= $vent->descuento ?>%</th>
        </tr>
        <tr>
            <th colspan="3">Total a pagar</th>
            <th><?= '$'.number_format($vent->totalizado,2) ?></th>
        </tr>
        <tr>
            <th colspan="3">Forma de pago</th>
            <th><?= $vent->forma_pago ?></th>
        </tr>
        <?php endif ?>
    </table>

    <div style="margin:30px">		
        <h3>Observaciones: </h3>
        <?= $vent->observaciones ?>
    </div>
    <div style="text-align: right">        
        <?php if($vent->read): ?>
            <a href="<?= base_url('expediente/expedientes/'.$cliente) ?>" class="btn btn-success">Atrás</a>
        <?php else: ?>            
            <?php if ($vent->status==0): ?>
                <button id="closebtn" class="btn btn-success">Cerrar venta</button>
            <?php endif ?>
        <?php endif ?>
        <a id="printbtn" style="<?= $vent->status==0 ? 'display:none' : ''; ?>" target='_blank' href="<?= base_url('reportes/rep/visualizarReporte/36/html/ventas_id/' . $venta->row()->ventas_id) ?>" class="btn btn-info" target="_new"><i class="fa fa-print"></i> Imprimir Ticket Ingreso</a>
        <a id="printbtn2" style="<?= $vent->status==0 ? 'display:none' : ''; ?>" target='_blank' href="<?= base_url('reportes/rep/visualizarReporte/40/html/ventas_id/' . $venta->row()->ventas_id) ?>" class="btn btn-info" target="_new"><i class="fa fa-print"></i> Imprimir Ticket Pago</a>
    </div>
</form>
<?php $this->load->view('predesign/chosen', array()); ?>
<script>
    var total = <?= str_replace(',','',str_replace('$','',$total)) ?>;
    $("#descuento").on('change',function(){
        var totalizado = parseFloat($(this).val());
        totalizado = total-((totalizado*total)/100);
        $("#totalizado").val(totalizado.toFixed(2));
    });
    function save() {
        if (confirm('¿Seguro que desea cerrar esta venta?')) {
            $("#printbtn").show();
            $("#printbtn2").show();
            $("#closebtn").attr('disabled', true);
            $.post('<?= base_url('procesos/cargar_productos/' . $cliente . '/add/update/' . $venta->row()->ventas_id) ?>', {
                status: 1,
                descuento:$("#descuento").val(),
                totalizado:$("#totalizado").val(),
                forma_pago:$("#forma_pago").val(),
            }, function (data) {
                $("#msj").html("La venta ha sido cerrada con éxito").addClass('alert alert-success');
            });
        }
        return false;
    }
</script>