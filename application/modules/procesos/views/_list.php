<?= $crud->header ?>
<div style="text-align: right">
	Total Venta: <?= $this->db->query('select IF(SUM(total) IS NULL,0,SUM(total)) as total FROM ventas_detalles where ventas_id='.$venta.' AND muestra = \'0\'')->row()->total ?>
</div>
<?= $crud->output ?>
<script>
	$(document).on('change','#field-productos_id',function(){
		$.post('<?= base_url('archivo/productos/json_list') ?>',{
			'search_text[]':$(this).val(),
			'search_field[]':"id"
		},function(data){
			data = JSON.parse(data);
			if(data.length>0){
				data = data[0];
				if($("#field-cantidad").val()===''){
					$("#field-cantidad").val(1);
				}
				$("#field-precio").val(data.precio_venta);
				$("#field-total").val(data.precio_venta);
				totalizar();
			}
		});
	});

	$(document).on("change","#field-cantidad, #field-precio",function(){
		totalizar();
	});

	function totalizar(){
		var precio = parseFloat($("#field-precio").val());
		var cantidad = parseFloat($("#field-cantidad").val());
		$("#field-total").val(precio*cantidad);
	}

	$("#field-total").attr('readonly',true);
</script>