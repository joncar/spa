<div><?= $crud->header ?></div>
<div id="msj"></div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title" style="font-size: 32px;">
            <?php if (empty($venta)): ?>
                Información de sesión
            <?php else: ?>
                Información de sesión #<?= $vent->id ?>
            <?php endif ?>
        </h1>
    </div>
    <div class="panel-body">
        <?php if (empty($venta)): ?>
            <?php $this->load->view('_formAddSesion'); ?>
        <?php else: ?>
            <?php $this->load->view('_formReadSesion'); ?>
        <?php endif ?>
    </div>
</div>