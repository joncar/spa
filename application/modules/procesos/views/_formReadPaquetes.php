<style>
    .chosen-container .chosen-drop{
        position:relative !important;
    }
</style>
<form onsubmit="return save(this)">
    <div class="row" style="margin: 30px">
        <div class="col-xs-12 col-sm-4"><b>Fecha de la sesión:</b> <?= date("d/m/Y") ?></div>
        <div class="col-xs-12 col-sm-4"><b>Atendido por:</b> <?= $this->db->get_where('user',array('id'=>$vent->user_id))->row()->nombre ?></div>
        <div class="col-xs-12 col-sm-4"><b>Sucursal:</b> <?= $this->user->sucursalnombre ?></div>
    </div>
    <div style="overflow: auto; width:100%;">
    <table class="table table-sorted table-responsive">
        <tr>
            <th>Descripción</th>
            <th>Cant.</th>
            <th>Uni. Medida</th>
            <th>Precio</th>
        </tr>	
        <tr>
            <td><?= $this->db->get_where('paquetes', array('id' => $vent->paquetes_id))->row()->nombre ?></td>
            <td><?= $vent->cantidad ?></td>
            <td><?= $vent->unidad_medida ?></td>
            <td><?= $vent->precio ?></td>
        </tr>
        <tr>
            <td colspan="3"><b>Descuento</b></td>
            <td>
                <?php if($vent->abono==0): ?>
                    <input type="text" id="descuento" name="descuento" value="<?= $vent->descuento ?>">%</td>
                <?php else: ?>
                    <?= $vent->descuento ?>%
                    <input type="hidden" name="descuento" id="descuento" value="<?= $vent->descuento ?>">
                <?php endif ?>
            <td></td>
        </tr>
        <?php if($vent->abono<$vent->totalizado): ?>
        <tr>
            <td colspan="3"><b>Total a pagar</b></td>
            <td>
                $<input type="text" id="totalizado" name="totalizado" value="<?= number_format($vent->totalizado,2) ?>" readonly>
            </td>
        </tr>
        <tr>
            <td colspan="3"><b>Deuda</b></td>
            <td>$<input type="text" value="<?= number_format($vent->totalizado-$vent->abono,2) ?>" readonly></td>
        </tr>
        <tr>
            <td colspan="3"><b>Abono</b></td>
            <td>$
                <input type="text" id="abono" name="abono" value="<?= number_format($vent->totalizado-$vent->abono,2) ?>">
            </td>
        </tr>        
        <tr>
            <td colspan="3"><b>Forma de pago</b></td>
            <td>
                <select id="forma_pago" name="forma_pago" class="form-control" data-placeholder='Seleccione una forma de pago'>
                    <option>Efectivo</option>
                    <option>Transferencia</option>
                    <option>Cheque</option>
                    <option>Tarjeta de Debito</option>
                    <option>Tarjeta de Crédito</option>
                </select>
            </td>
        </tr>
        <?php else: ?>
        <tr>
            <td colspan="3"><b>Total paquete</b></td>
            <td>
                <?= $vent->totalizado ?>
            </td>
        </tr>
        <tr>
            <td colspan="3"><b>Forma de pago</b></td>
            <td>
                <?= $vent->forma_pago ?>
            </td>
        </tr>
        <?php endif ?>
    </table>
    
</div>
    
    <div style="margin:30px">		
        <textarea name="observaciones" id="observaciones" class="form_control" style="width:100%;" placeholder="Observaciones" data-placeholder="Observaciones"></textarea>
    </div>
    <?php if($vent->abono>0): ?>
        <div style="text-align: right">
            <a href="<?= base_url('reportes/rep/visualizarReporte/41/html/id/' . $vent->id) ?>" target="_new" class="btn btn-info"><i class="fa fa-print"></i> Imprimir Ticket Ingreso</a>
            <a href="<?= base_url('reportes/rep/visualizarReporte/42/html/id/' . $vent->id) ?>" target="_new" class="btn btn-info"><i class="fa fa-print"></i> Imprimir Ticket Pago</a>
        </div>
    <?php endif ?>
    <?php if($vent->abono<$vent->totalizado): ?>
        <div style="text-align: right">
            <button type="submit" class="btn btn-info" id="closebtn">Aplicar paquete</button>            
        </div>
    <?php endif ?>
</form>
<?php $this->load->view('predesign/chosen', array()); ?>
<script>
    $(document).on('ready', function () {
        $(".chosen-container").css('width', '100%');
        $("#cantidad,#descuento").on('change',function(){calcular()});
    });

    function calcular(){        
        var total = <?= $vent->precio-$vent->abono ?>;
        var descuento = parseFloat($("#descuento").val());
        var totalizado = total - ((descuento*total)/100);
        $("#totalizado").val(totalizado);
        $("#abono").val(totalizado);
    }
    function save(f) {
        if (confirm('¿Seguro que desea cerrar esta venta?')) {            
            var abonado = <?= $vent->abono ?>;
            var total = $("#totalizado").val();            
            total = total.replace(',','');
            total = total.replace('$','');
            total = parseFloat(total);
            var abono = parseFloat($("#abono").val())+abonado;
            if(!isNaN(abono)){                
                if(abono>total){ 
                    alert("Disculpe, el abono no puede ser mayor a la deuda");
                    $("#abono").val(total-abonado);
                }else{
                    var form = new FormData(f);                    
                    $("#closebtn").attr('disabled', true);
                    $.ajax({
                        url: '<?= base_url('procesos/clientes_paquetes/' . $cliente . '/add/update/' . $vent->id) ?>',
                        data: form,
                        context: document.body,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST' 
                    }).always(function (data) {
                        data = data.replace('<textarea>', '');
                        data = data.replace('</textarea>', '');
                        data = JSON.parse(data);
                        if (data.success) {
                            document.location.reload();
                        } else {
                            $("#msj").html("Complete todos los datos antes de enviarlos").addClass('alert alert-danger');
                        }
                    });
                    return false;
                }
            }
        }
        return false;
    }
</script>