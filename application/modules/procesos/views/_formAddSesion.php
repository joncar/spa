<?php
$this->db->select('productos.*');
$this->db->where('productos.grupo = 1 AND productos.sucursales_id = '.$this->user->sucursal.' OR (productos.tipo = 1 AND productos.sucursales_id = ' . $this->user->sucursal . ' AND productos.stock > 0)', NULL, TRUE);
$this->db->order_by('productos.nombre','ASC');
$productos_servicios = $this->db->get('productos');

$this->db->select('productos.*');
$this->db->where('productos.grupo = 2 AND productos.tipo = 1 AND productos.sucursales_id = ' . $this->user->sucursal . ' AND productos.stock > 0', NULL, TRUE);
$this->db->order_by('productos.nombre','ASC');
$productos = $this->db->get('productos');

$this->db->select('productos.*');
$this->db->where('productos.tipo = 2 AND productos.sucursales_id = ' . $this->user->sucursal . ' AND productos.stock > 0', NULL, TRUE);
$this->db->order_by('productos.nombre','ASC');
$usos = $this->db->get('productos');
?>
<style>
    .chosen-container .chosen-drop{
        position:relative !important;
    }
</style>
    <form onsubmit="return save(this)">
            <div class="row" style="margin: 30px">
            <div class="col-xs-12 col-sm-4"><b>Fecha de la sesión:</b> <?= date("d/m/Y") ?></div>
            <div class="col-xs-12 col-sm-4"><b>Atendido por:</b> <?= $this->user->nombre ?></div>
            <div class="col-xs-12 col-sm-4"><b>Sucursal:</b> <?= $this->user->sucursalnombre ?></div>
        </div>
        <div style="overflow: auto; width:100%;">
        <table class="table table-sorted table-responsive" style="max-width:100%;">
            <tr>
                <th>Descripción</th>
                <th>Cant.</th>
                <th>Uni. Medida</th>
                <th>Precio</th>
                <th>Total</th>
            </tr>		
            <tr>
                <td><?= form_dropdown_from_query('detalles[productos_id][]', $productos_servicios, 'id', 'nombre', 0, '', TRUE, 'productos') ?>(Comprado)</td>
                <td><input type="text" name="detalles[cantidad][]" placeholder="0" class="cantidad form_control"></td>
                <td><input type="text" name="detalles[unidad_medida][]" value="0" class="unidad_medida form_control" readonly=""></td>
                <td>
                    <input type="text" name="detalles[precio][]" value="0" class="precio form_control" readonly="">
                    <input type="hidden" name="detalles[muestra][]" value="0">
                    <input type="hidden" name="detalles[tratamiento][]" value="0">
                    <input type="hidden" class="stock" value="0">
                </td>
                <td>
                    <input type="text" value="0" class="total_venta form_control" readonly="">
                    <button type="button" class="addRow btn btn-primary btn-small"><i class="fa fa-plus"></i></button>
                    <button type="button" class="remRow btn btn-danger btn-small" style="display:none"><i class="fa fa-minus"></i></button>
                </td>
            </tr>
            <tr class="noprice">
                <td><?= form_dropdown_from_query('detalles[productos_id][]', $productos, 'id', 'nombre', 0, '', TRUE, 'productos') ?>(Muestra)</td>
                <td><input type="text" name="detalles[cantidad][]" placeholder="0" class="cantidad form_control"></td>
                <td><input type="text" name="detalles[unidad_medida][]" value="0" class="unidad_medida form_control" readonly=""></td>
                <td>
                    <input type="text" name="detalles[precio][]" value="0" class="precio form_control" readonly="">
                    <input type="hidden" name="detalles[muestra][]" value="1">
                    <input type="hidden" name="detalles[tratamiento][]" value="0"> 
                    <input type="hidden" class="stock" value="0">
                </td>
                <td>
                    <input type="text" value="0" class="total_venta form_control" readonly="">
                </td>
            </tr>
            <tr class="noprice utilizadoRow">
                <td><?= form_dropdown_from_query('detalles[productos_id][]', $usos, 'id', 'nombre', 0, '', TRUE, 'productos') ?>(Utilizado)</td>
                <td><input type="text" name="detalles[cantidad][]" placeholder="0" class="cantidad form_control" readonly="" ></td>
                <td><input type="text" name="detalles[unidad_medida][]" placeholder="aplicaciòn" class="unidad_medida form_control"></td>
                <td>
                    <input type="text" name="detalles[precio][]" value="0" class="precio form_control" readonly="">
                    <input type="hidden" name="detalles[muestra][]" value="0" class="muestra">
                    <input type="hidden" name="detalles[tratamiento][]" value="1" class="tratamiento">
                    <input type="hidden" class="stock" value="0">
                </td>	
                <td>
                    <input type="text" value="0" class="total_venta form_control" readonly="">
                    <button type="button" class="addRow btn btn-primary btn-small"><i class="fa fa-plus"></i></button>
                    <button type="button" class="remRow btn btn-danger btn-small" style="display:none"><i class="fa fa-minus"></i></button>
                </td>
            </tr>		
        </table>
            
</div>
        <div style="margin:30px">		
            <textarea name="observaciones" id="observaciones" class="form_control" style="width:100%;" placeholder="Observaciones" data-placeholder="Observaciones"></textarea>
        </div>
        <div style="text-align: right">
            <?php 
                $this->db->select('clientes_paquetes.*, paquetes.nombre');
                $this->db->join('paquetes','paquetes.id = clientes_paquetes.paquetes_id');
                $paquetes = $this->db->get_where('clientes_paquetes',array('clientes_id'=>$cliente,'status'=>1,'disponibles >'=>0,'paquetes.sucursales_id'=>$this->user->sucursal));
                if($paquetes->num_rows()>0): 
            ?>
            Descontar paquete: 
                <select name='paquetes_id'>
                    <option value=''>No descontar</option>
                    <?php 
                        foreach($paquetes->result() as $p): 
                    ?>  
                    <option value='<?= $p->id ?>'><?= $p->nombre ?></option>
                    <?php endforeach ?>
                </select>
            <?php endif ?>
            <button class="btn btn-success">Guardar Sesión</button>
        </div>
    </form>
<?php $this->load->view('predesign/chosen', array()); ?>
<script>
    $(document).on('ready', function () {
        $(".chosen-container").css('width', '100%');
    });

    $(document).on('change',".cantidad", function () {
        var row = $(this).parents('tr');
        var cantidad = parseFloat($(this).val());
        if(!isNaN(cantidad) && !$(this).parents('tr').hasClass('noprice')){
            var stock = parseFloat(row.find('.stock').val());
            var precio = row.find('.precio').val();        
            precio = precio.replace(/\,/g,'');
            precio = precio.replace(/\./g,'.');
            precio = precio.replace(/\$/g,'');
            precio = parseFloat(precio);
            var total = precio * cantidad;
            if (cantidad > stock && stock > 0) {
                $(this).val(stock);
                cantidad = stock;
                alert("La cantidad elegida para este elemento ha excedido las existencias, se ha modificado al maximo de existencias disponibles en el stock de inventario");
            }        
            row.find('.total_venta').val(total);
        }else{
            row.find('.total_venta').val(0);
        }
    });

    $(document).on("change",".productos", function () {
        var row = $(this).parents('tr');
        if ($(this).val() !== '') {
            $.post('<?= base_url("procesos/productos/consultar_precios/json_list") ?>', {
                'search_text[]': $(this).val(),
                'search_field[]': 'id'
            }, function (data) {
                var data = JSON.parse(data);
                data = data[0];
                if(!row.hasClass('noprice')){
                    row.find('.cantidad').val(1);
                    row.find('.precio').val(data.precio_venta);
                    row.find('.unidad_medida').val(data.unidad_medida);
                    row.find('.stock').val(data.stock);
                    row.find(".cantidad").trigger('change');
                }
            });
        }
    });
    
    $(document).on("click",".addRow",function(){
        var row = $(this).parents('tr');
        var newRow = $(row.clone());
        newRow.find('input[type="text"]').val('');
        newRow.find('.remRow').show();        
        row.after(newRow);
        newRow.find('.chosen-container').remove();
        newRow.find('select').chosen();
        newRow.find('.chosen-container').css('width','100%');        
    });
    
    $(document).on("click",".remRow",function(){
        var row = $(this).parents('tr');        
        row.remove();
    });

    function save(f) {
        var form = new FormData(f);
        form.append('clientes_id',<?= $cliente ?>);
        form.append('user_id',<?= $this->user->id ?>);
        form.append('sucursales_id',<?= $this->user->sucursal ?>);
        form.append('fecha_venta', '<?= date("Y-m-d H:i:s") ?>');
        form.append('total_venta', 0);
        form.append('status', 0);
        $("#msj").html("").removeClass('alert alert-success alert-danger');
        $.ajax({
            url: '<?= base_url('procesos/cargar_productos/' . $cliente . '/add/insert') ?>',
            data: form,
            context: document.body,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST'
        }).always(function (data) {
            data = data.replace('<textarea>', '');
            data = data.replace('</textarea>', '');
            data = JSON.parse(data);
            if (data.success) {
                $("#msj").html("La sesión ha sido almacenada con éxito").addClass('alert alert-success');
                $(".panel-default input").val('');
                $(".panel-default select").val("");
                $(".panel-default select").trigger("chosen:updated");
                //document.location.href="<?= base_url('procesos/ventas/success') ?>";
            } else {
                $("#msj").html("Complete todos los datos antes de enviarlos").addClass('alert alert-danger');
            }
        });
        return false;
    }
</script>