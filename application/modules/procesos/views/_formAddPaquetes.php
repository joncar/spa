<?php
$this->db->select('paquetes.*');
$this->db->order_by('paquetes.nombre','ASC');
$this->db->where('paquetes.sucursales_id',$this->user->sucursal);
$productos = $this->db->get('paquetes');
?>
<style>
    .chosen-container .chosen-drop{
        position:relative !important;
    }
</style>
<form onsubmit="return save(this)">
    <div class="row" style="margin: 30px">
        <div class="col-xs-12 col-sm-4"><b>Fecha de la sesión:</b> <?= date("d/m/Y") ?></div>
        <div class="col-xs-12 col-sm-4"><b>Atendido por:</b> <?= $this->user->nombre ?></div>
        <div class="col-xs-12 col-sm-4"><b>Sucursal:</b> <?= $this->user->sucursalnombre ?></div>
    </div>
    <div style="overflow: auto; width:100%;">
    <table class="table table-sorted table-responsive">
        <tr>
            <th>Descripción</th>
            <th>Cant.</th>
            <th>Uni. Medida</th>
            <th>Precio</th>
        </tr>	
        <tr>
            <td><?= form_dropdown_from_query('paquetes_id', $productos, 'id', 'nombre', 0, '', TRUE, 'productos') ?></td>
            <td><input type="text" id='cantidad' name="cantidad" placeholder="0" class="cantidad form_control"></td>
            <td><input type="text" name="unidad_medida" value="0" class="unidad_medida form_control" readonly=""></td>
            <td>
                <input type="hidden" class="disponibles" name="disponibles" id="disponibles" value="0">
                <input type="text" id="precio" name="precio" value="0" class="precio form_control" readonly="">
            </td>
        </tr>
    </table>
    
</div>
    
    <div style="margin:30px">		
        <textarea name="observaciones" id="observaciones" class="form_control" style="width:100%;" placeholder="Observaciones" data-placeholder="Observaciones"></textarea>
    </div>
    <div style="text-align: right">
        <input type="hidden" name="descuento" id="descuento">
        <input type="hidden" name="totalizado" id="totalizado">
        <button class="btn btn-success">Aplicar paquete</button>
    </div>
</form>
<?php $this->load->view('predesign/chosen', array()); ?>
<script>
    $(document).on('ready', function () {
        $(".chosen-container").css('width', '100%');

        $("#cantidad,#descuento").on('change',function(){calcular()});
    });

    function calcular(){
        var total = 0;
        var cantidad = parseFloat($("#cantidad").val());
        var descuento = parseFloat($("#descuento").val());
        descuento = isNaN(descuento)?0:descuento;
        var precio = $("#precio").val();
        precio = precio.replace(',','');
        precio = precio.replace('$','');
        total = (cantidad*precio);
        total = total-((descuento*total)/100);
        $("#totalizado").val(total);
        $("#abono").val(total);
    }



    $(".productos").on("change", function () {
        var row = $(this).parents('tr');
        if ($(this).val() !== '') {
            $.post('<?= base_url("procesos/productos/consultar_paquetes/json_list") ?>', {
                'search_text[]': $(this).val(),
                'search_field[]': 'id'
            }, function (data) {
                var data = JSON.parse(data);
                data = data[0];
                row.find('.cantidad').val(1);
                row.find('.precio').val(data.precio_venta);
                row.find('.unidad_medida').val(data.unidad_medida);
                row.find('#disponibles').val(data.cantidad);
                calcular();
            });
        }
    });

    function save(f) {
        var form = new FormData(f);
        form.append('clientes_id',<?= $cliente ?>);
        form.append('user_id',<?= $this->user->id ?>);
        form.append('sucursales_id',<?= $this->user->sucursal ?>);
        form.append('fecha_venta', '<?= date("Y-m-d H:i:s") ?>');
        form.append('total_paquete', 0);
        form.append('status', 1);        
        $("#msj").html("").removeClass('alert alert-success alert-danger');
        $.ajax({
            url: '<?= base_url('procesos/clientes_paquetes/' . $cliente . '/add/insert') ?>',
            data: form,
            context: document.body,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST' 
        }).always(function (data) {
            data = data.replace('<textarea>', '');
            data = data.replace('</textarea>', '');
            data = JSON.parse(data);
            if (data.success) {
                $("#msj").html("La sesión ha sido almacenada con éxito").addClass('alert alert-success');
                $(".panel-default input").val('');
                $(".panel-default select").val("");
                $(".panel-default select").trigger("chosen:updated");
                document.location.href="<?= base_url('procesos/clientes_paquetes/') ?>/";
            } else {
                $("#msj").html("Complete todos los datos antes de enviarlos").addClass('alert alert-danger');
            }
        });
        return false;
    }
</script>