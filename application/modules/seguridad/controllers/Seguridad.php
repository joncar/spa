<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Seguridad extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function grupos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $crud->set_relation_n_n('funciones','funcion_grupo','funciones','grupo','funcion','{nombre}');            
            $crud->set_relation_n_n('miembros','user_group','user','grupo','user','{nombre} {apellido}');
            $output = $crud->render();
            $this->loadView($output);
        }               
        
        function funciones($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function user($x = '',$y = ''){
            $this->norequireds = array('apellido_materno','foto');
            $crud = $this->crud_function($x,$y);  
            $crud->field_type('status','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('fecha_registro','hidden',date("Y-m-d"));
            $crud->field_type('fecha_actualizacion','hidden',date("Y-m-d"));
            $crud->field_type('admin','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('estado_civil','enum',array('soltero'=>'Soltero','casado'=>'Casado','divorciado'=>'Divorciado','viudo'=>'Viudo'));
            $crud->field_type('sexo','enum',array('M'=>'M','F'=>'F'));
            $crud->field_type('password','password');
            $crud->field_type('repetir_password','password');  
            $crud->set_relation_n_n('sucursales','user_sucursales','sucursales','user_id','sucursales_id','nombre');
            if($crud->getParameters()=='add'){
                $crud->set_rules('repetir_password','Repetir Password','required');            
                $crud->set_rules('cedula','Cedula','required|is_unique[user.cedula]');
            }
            $crud->unset_columns('password','');
            $crud->unset_fields('fecha_registro','fecha_actualizacion');
            $crud->set_field_upload('foto','img/fotos');
            if($crud->getParameters()=='edit' || $crud->getParameters()=='update' || $crud->getParameters()=='update_validation'){
                //$crud->field_type('cedula','hidden');
            }
            $crud->callback_before_insert(function($post){
                $this->pass = $post['password'];
                $post['password'] = md5($post['password']);                
                return $post;
            });
            $crud->callback_after_insert(function($post,$primary){
                //Enviar correos
                $notif = $post['admin']==1?3:2;
                $datos = (object)$post;
                $datos->cajero = get_instance()->user->nombre;
                $datos->permisos = '';
                $datos->password2 = $this->pass;
                if(!empty($_POST['grupos'])){
                    foreach($_POST['grupos'] as $p){
                        get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>$p));
                        $datos->permisos.= $this->db->get_where('grupos',array('id'=>$p))->row()->nombre.'<br/>';
                    }
                }

                get_instance()->enviarcorreo((object)$datos,3,'admin@taketen.com.mx');
                get_instance()->enviarcorreo((object)$datos,2);
            });
            $crud->callback_before_update(function($post,$primary){
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);
                get_instance()->db->delete('user_group',array('user'=>$primary));
                if(!empty($_POST['grupos'])){                    
                    foreach($_POST['grupos'] as $p){
                        get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>$p));
                    }
                }
                return $post;
            });
            if($x=='add' || $x=='edit'){
                $crud->fields('nombre','apellido','email','password','fecha_registro','fecha_actualizacion','status','admin','foto','sucursales','grupos');
            }
            if($x=='edit' && is_numeric($y)){
                $this->edit = $y;
            }else{
                $this->edit = -1;
            }
            $crud->callback_field('grupos',function($val){
                //form_dropdown_from_query('grupos[]','grupos','id','nombre','','multiple="multiple"',FALSE,'chosen-multiple-select','Seleccione un grupo');
                $misgrupos = array();
                if(!empty($this->edit)){
                    $this->db->select('grupos.*');
                    $this->db->join('grupos','grupos.id = user_group.grupo');
                    foreach($this->db->get_where('user_group',array('user'=>$this->edit))->result() as $g){
                        $misgrupos[] = $g->nombre;
                    }
                }
                $sel = '<select style="width:100%" name="grupos[]" multiple="multiple" class="chosen-multiple-select" data-placeholder="Seleccione los grupos">';
                foreach($this->db->get('grupos')->result() as $g){
                    $selected = in_array($g->nombre,$misgrupos)?'selected="selected"':"";
                    $sel .= '<option value="'.$g->id.'" '.$selected.'>'.$g->nombre.'</option>';
                }
                $sel .= '</select>';                                
                return $sel;
            });
            $crud->columns('foto','nombre','apellido','email','status');
            $output = $crud->render();
            $this->loadView($output);
        }

        function acciones($x = '',$y = ''){            
            $crud = $this->crud_function($x,$y);            
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function perfil($x = '',$y = ''){
            $this->as['perfil'] = 'user';
            $crud = $this->crud_function($x,$y);    
            $crud->where('id',$this->user->id);
            $crud->fields('cedula','nombre','apellido_paterno','apellido_materno','email','password','foto');
            $crud->field_type('password','password');
            $crud->callback_field('cedula',function($val){
                return form_input('cedula',$val,'id="field-cedula" class="form-control" disabled="true"');
            });
            $crud->field_type('foto','image',array('path'=>'img/fotos','width'=>'300px','height'=>'300px'));
            $crud->callback_before_update(function($post,$primary){
                if(!empty($primary) && $this->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary)){
                    $post['password'] = md5($post['password']);                
                }
                return $post;
            });
            $crud->callback_after_update(function($post,$id){
                $this->user->login_short($id);
            });
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function user_insertion($post,$id = ''){
            if(!empty($id)){
                $post['pass'] = $this->db->get_where('user',array('id'=>$id))->row()->password!=$post['password']?md5($post['password']):$post['password'];
            }
            else $post['pass'] = md5($post['pass']);
            return $post;
        }
    }
?>
