<footer id="main-footer">
    <div class="inner-container container">
        <div class="t-sec clearfix">
            <div class="widget-box col-sm-6 col-md-3"  >
                <a href="#" id="f-logo">
                    <!--                    <span class="title">Colosseum</span>-->
                    <!--                    <span class="desc">Luxury Hotel</span>-->
                    <div class="">
                        <img src="<?= base_url() ?>img/logo.png" style=" margin-top: -19px; width: 180px"alt="cal fuster de la plaça">
                    </div> 
                </a>

                <div class="widget-content text-widget">
                    Situada en mig del casc antic de Sant Martí de Tous, vila mil·lenària plena d’històries, llegendes i paratges encantadors, la casa recull l’encant i el confort que el lloc proporciona. Pedra, fusta i materials nobles, s’hi combinen vestigis gòtics amb moderns detalls que la fan còmode i confortable.
                </div>
            </div>
            <div class="widget-box col-sm-6 col-md-3" >
                <h4 class="title">Newsletter</h4>
                <div class="widget-content newsletter">
                    <div id="subsmessage"></div>
                    <div class="desc">
                        Si vols rebre informació puntual d'activitats i ofertes suscriu-te al nostre butlletí.
                    </div>
                    <form class="news-letter-form" onsubmit="return subscribir()">
                        <input type="email" id="emailSub" name="email" class="email" placeholder="Email">
                        <button type="submit" class="ravis-btn btn-type-2">Inscriu-te</button>
                    </form>
                </div>
            </div>
            <div class="widget-box col-sm-6 col-md-3" >
                <h4 class="title">Últimes notícies</h4>
                <div class="widget-content latest-posts">
                    <ul>
                        <?php $this->db->limit(2); $this->db->order_by('fecha','DESC'); ?>
                        <?php foreach($this->db->get('blog')->result() as $b): ?>
                            <li class="clearfix">
                                <div class="img-container col-xs-4">
                                    <a href="<?= base_url().'blog/'.toURL($b->id.'-'.$b->titulo) ?>">
                                        <img src="<?= base_url().'img/blog/'.$b->foto ?>" alt="<?= $b->titulo ?>">
                                    </a>
                                </div>
                                <div class="desc-box col-xs-8">
                                    <a href="<?= base_url().'blog/'.toURL($b->id.'-'.$b->titulo) ?>" class="title">
                                        <?= $b->titulo ?>
                                    </a>
                                    <div class="desc">
                                        <?= cortar_palabras(strip_tags($b->texto),4) ?>
                                    </div>
                                    <a href="<?= base_url().'blog/'.toURL($b->id.'-'.$b->titulo) ?>" class="read-more">
                                        Veure més
                                    </a>

                                </div>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
            <div class="widget-box col-sm-6 col-md-3" >
                <h4 class="title">Contacta'ns</h4>
                <div class="widget-content contact">
                    <ul class="contact-info">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="https://www.google.com/maps/dir/41.5598075,1.5240244/41.5598196,1.5240205/@41.5599276,1.5239612,171m/data=!3m1!1e3!4m2!4m1!3e0?hl=es-ES"> Pl. Manel Mª Girona, 3 Sant Martí de Tous (Barcelona) </a>
                        </li>
                        <li>
                            <i class="fa fa-phone"></i>
                            <a href="tel:938 096 428">938096428</a>  /
                            <a href="tel:651 807 304  ">651807304  </a>
                        </li>
                        <li>
                            <i class="fa fa-envelope"></i>
                            <a href="mailto:fuster@calfusterdetous.com"> fuster@calfusterdetous.com</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="b-sec clearfix">
            <div class="copy-right">
                Amb <i class="fa fa-heart"></i> per <a href="http://hipo.tv" target="_blank">Hipo</a> © 2018 /
                <a href="<?= base_url() ?>p/avis_legal.html" target="_blank"> Avis Legal</a>  -
                <a href="<?= base_url() ?>p/condicions.html" target="_blank"> Condicions de Reserva</a>  -
                <a href="<?= base_url() ?>p/privacitat.html" target="_blank"> Política de Privacitat</a> -
                <a href="<?= base_url() ?>p/cookies.html" target="_blank"> Cookies</a>
            </div>


            <ul class="social-icons list-inline">
                <li> <img src="<?= base_url() ?>img/client-logo/altria1.png" style=" width: 51px" alt="Client Logo"></a></li>
                <li> <img src="<?= base_url() ?>img/client-logo/bluehost.png" style=" width: 51px" alt="Client Logo"></a></li>
                <li> <img src="<?= base_url() ?>img/client-logo/cube.png" style=" width: 51px" alt="Client Logo"></a></li>
                <li> <img src="<?= base_url() ?>img/client-logo/erikaschesonis1.png" style=" width: 51px" alt="Client Logo"></a></li>
                <li> <img src="<?= base_url() ?>img/client-logo/modernart.png" style=" width: 51px" alt="Client Logo"></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            </ul>

        </div>
    </div>
</footer>
