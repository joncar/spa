<?php if(!empty($this->user->sucursal)): ?>
<div class="row" style="margin:40px;">
    <div class="space-6"></div>

    <div class="col-sm-12 infobox-container">
        <div class="infobox infobox-green">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-shopping-cart"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $this->db->get_where('ventas',array('fecha_venta'=>date("Y-m-d"),'sucursales_id'=>$this->user->sucursal))->num_rows() ?></span>
                <div class="infobox-content">Ventas realizadas</div>
            </div>
        </div>

        <div class="infobox infobox-green">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-user"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $this->db->query(' SELECT COUNT(id) as total from clientes')->row()->total ?></span>
                <div class="infobox-content">Clientes</div>
            </div>
        </div>

        <div class="infobox infobox-blue">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-money"></i>
            </div>

            <div class="infobox-data">
                <?php 
                    $sesiones_pendientes = $this->db->query(' SELECT IF(SUM(total) IS NULL,0,SUM(total)) as total from ventas_dia where sucursales_id='.$this->user->sucursal.' AND status = \'0\' AND DATE(Fecha) = \''.date("Y-m-d").'\'')->row()->total; 
                ?>
                <span class="infobox-data-number">$<?= number_format($sesiones_pendientes,2) ?></span>
                <div class="infobox-content">Sesiones por cobrar</div>
            </div>
        </div>

        <div class="infobox infobox-blue">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-money"></i>
            </div>

            <div class="infobox-data">
                <?php $sesiones_cobradas = $this->db->query(' SELECT IF(SUM(total) IS NULL,0,SUM(total)) as total from ventas_dia where sucursales_id='.$this->user->sucursal.' AND status = \'1\' AND DATE(Fecha) = \''.date("Y-m-d").'\'')->row()->total; ?>
                <span class="infobox-data-number">$<?= number_format($sesiones_cobradas,2) ?></span>
                <div class="infobox-content">Ventas cobradas</div>
            </div>
        </div>

        <div class="infobox infobox-blue">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-money"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number">$<?= number_format($sesiones_pendientes+$sesiones_cobradas,2) ?></span>
                <div class="infobox-content">Total Ventas</div>
            </div>
        </div>

        
    </div> 
    <div class="vspace-12-sm"></div>
    </div> 
    <div class="row" style="margin:30px">
    <h1>Ultimas sesiones</h1>
    <div class="col-sm-12">
        <?php
            $crud = new ajax_grocery_crud();
            $crud->set_theme('bootstrap2')
                 ->set_table('ventas')
                 ->set_subject('expedientes')
                 ->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->unset_export()
                 ->limit(5)
                 ->field_type('status','true_false',array('0'=>'Pendiente','1'=>'Pagado'))
                 ->order_by('id','DESC')
                 ->display_as('id','#Sesión')
                 ->display_as('fecha_venta','Fecha')
                 ->display_as('clientes_id','Cliente')
                 ->display_as('user_id','Atendido por')
                 ->display_as('status','Estatus')
                 ->columns('fecha_venta','id','clientes_id','user_id','status');
            $crud->set_url('reportes/rep/ultimas_sesiones/');
            $crud = $crud->render();
            echo $crud->output;

        ?>
    </div><!-- /.col -->
</div>


<?php endif ?>