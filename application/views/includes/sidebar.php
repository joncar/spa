<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li>
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
             
             <?php 
                    $menu = array(                        
                        'procesos'=>array('ventas','ventas_cobrar','clientes_paquetes','paquetes_asignados'),
                        'expediente'=>array('expedientes','diagnosticos'),
                        /*'inventarios'=>array('inventario','compras'),*/
                        'reportes'=>array(
                            'rep/newreportes',
                            'rep/report_organizer',
                            'rep/ver_reportes',
                            'rep/mis_reportes',
                        ),                        
                        'archivo'=>array('clientes','administrar_diagnosticos','productos','paquetes','sucursales','sesiones'),
                        'notificaciones'=>array('admin/notificaciones'),
                        'seguridad'=>array('acciones','grupos','funciones','user')
                    );
                    $menu = $this->user->filtrarMenu($menu);
                    $label = array(
                        'compras'=>array('Cargar inventario','fa fa-book'),
                        'clientes_paquetes'=>array('Asignar paquetes'),
                        'archivo'=>array('Mantenimiento','fa fa-wrench'),
                        'ventas'=>array('Añadir sesión',''),
                        'inventarios'=>array('Inventario','fa fa-cubes'),
                        'ventas_cobrar'=>array('Sesiones por cobrar'),
                        'expediente'=>array('Expedientes','fa fa-address-card'),
                        'procesos'=>array('Tratamientos','fa fa-medkit'),
                        'notificaciones'=>array('Notificaciones','fa fa-bullhorn'),                        
                        'reportes'=>array('Reportes','fa fa-files-o'),
                        'mis_reportes'=>array('Imprimir reportes'),
                        'ver_reportes'=>array('Ver reportes'),
                        'administrar_diagnosticos'=>array('Diagnosticos'),
                        'seguridad'=>array('Seguridad','fa fa-user-secret')
                    );
             ?>
             <?php  echo getMenu($menu,$label); ?>            
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>
        <div style="color:white; background:#1C1B1A; font-size:8px; text-align:center">
            <a href="http://sistembux.com/" style="color:white;">
                <?= img('img/minilogo.png','width:30%') ?>
            </a>
        </div>

        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
