<div class="login-dark">            
    <form method="post" action="<?= base_url('registro/forget') ?>" onsubmit="return validar(this)">
        <h2 class="sr-only">Recuperar password</h2>
        <div class="illustration">
            <img class="img-responsive" src="<?= base_url() ?>img/logo_taketen.png">
        </div>
        <div class="form-group">
            <input type="email" name="email" id="email" data-val="required" class="form-control" value="<?= $_SESSION['email'] ?>" readonly><br/>
        </div>
        <div class="form-group">
            <input type="password" class="form-control" name="pass" id="pass" placeholder="Nuevo Password"><br/>
        </div>
        <div class="form-group">
            <input type="password" class="form-control" name="pass2" id="pass2" placeholder="Repetir Password"><br/>
        </div>
        <input type="hidden" name="key" value="<?= $key ?>">
        <?php if (!empty($_SESSION['msj'])) echo $_SESSION['msj'] ?>
        <?php if (!empty($msj)) echo $msj ?>
        <div class="form-group">
            <button class="btn btn-primary btn-block" type="submit">Recuperar </button>            
        </div>            
        <a href="<?= base_url() ?>" class="forgot">Volver</a>
</div>
<?php $_SESSION['msj'] = null ?>