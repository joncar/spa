<?php if (empty($_SESSION['user'])): ?>
    <div class="login-dark">            
        <form method="post" action="<?= base_url('main/login') ?>">
            <h2 class="sr-only">Login Form</h2>
            <div class="illustration">
                <img class="img-responsive" src="<?= base_url() ?>img/logo_taketen.png">
            </div>
            <div class="form-group">
                <input class="form-control" type="email" name="email" placeholder="Correo@taketen.com.mx">
            </div>
            <div class="form-group">
                <input class="form-control" type="password" name="pass" placeholder="Contraseña">
            </div>
            <?php if (!empty($_SESSION['msj'])) echo $_SESSION['msj'] ?>
            <?php if (!empty($msj)) echo $msj ?>
            <div class="form-group">
                <input type="hidden" name="redirect" value="<?= empty($_GET['redirect']) ? base_url('panel') : base_url($_GET['redirect']) ?>">
                <button class="btn btn-primary btn-block" type="submit">Entrar </button>
            </div>
            <a href="<?= base_url('registro/forget') ?>" class="forgot">Recuperar contraseña</a></form>
    </div>

<?php else: ?>
    <div align="center"><a href="<?= base_url('panel') ?>" class="btn btn-success btn-large" style=" width: auto; padding-top: 20px">Entrar en el sistema</a></div>
<?php endif; ?>
<?php $_SESSION['msj'] = null ?>