<!Doctype html>
<html lang="es">
    <head>
            <title><?= empty($title)?'SysSpa':$title.'' ?></title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">            
            <?php if(empty($crud) || empty($css_files) || !empty($loadJquery)): ?>
            <script src="<?= base_url('js') ?>/jquery-1.10.0.js"></script>      
            <script src="<?= base_url('js/dist/js') ?>/bootstrap.min.js"></script>                
            <link rel="stylesheet" type="text/css" href="<?= base_url('js/dist/css') ?>/bootstrap.min.css">
            <?php endif ?>
            <?php 
            if(!empty($css_files) && !empty($js_files)):
            foreach($css_files as $file): ?>
            <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
            <?php endforeach; ?>
            <?php foreach($js_files as $file): ?>
            <script src="<?= $file ?>"></script>
            <?php endforeach; ?>                
            <?php endif; ?>        
            <link rel="stylesheet" href="<?= base_url() ?>css/login.css">
    </head>  
    <?php $this->load->view($view) ?>                  
</html>
