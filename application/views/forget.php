<div class="login-dark">            
    <form method="post" action="<?= base_url('registro/forget') ?>" onsubmit="return validar(this)">
        <h2 class="sr-only">Recuperar password</h2>
        <div class="illustration">
            <img class="img-responsive" src="<?= base_url() ?>img/logo_taketen.png">
        </div>
        <div class="form-group">
            <input class="form-control" type="email" name="email" placeholder="Correo@taketen.com.mx">
        </div>
        <?php if (!empty($_SESSION['msj'])) echo $_SESSION['msj'] ?>
        <?php if (!empty($msj)) echo $msj ?>
        <div class="form-group">
            <button class="btn btn-primary btn-block" type="submit">Recuperar </button>            
        </div>            
        <a href="<?= base_url() ?>" class="forgot">Volver</a>
</div>
<?php $_SESSION['msj'] = null ?>