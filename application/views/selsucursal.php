<div class="login-dark">                
    <form method="post" onsubmit="return false">
        <h2 class="sr-only">Seleccione una sucursal</h2>
        <div class="illustration">
            <img class="img-responsive" src="<?= base_url() ?>img/logo_taketen.png">
            <h1 class="text-danger">Sucursal </h1>
        </div>
        <div class="form-group">
            <label>Seleccione una sucursal</label>
            <select  autofocus="" name='sucursal' id='sucursal' class="form-control" data-placeholder='Seleccione una sucursal'>
                <option>Selecciona sucursal</option>
                <?php 
                    $this->db->select('sucursales.*');
                    $this->db->join('sucursales','sucursales.id = user_sucursales.sucursales_id');
                    foreach($this->db->get_where('user_sucursales',array('user_id'=>$this->user->id))->result() as $s):
                ?>
                <option value="<?= $s->id ?>"><?= $s->nombre ?></option>
                <?php 
                    endforeach
                ?>
            </select>
        </div>
    </form>
</div>
<script>
    $(document).on('change','#sucursal',function(){
        document.location.href="<?= base_url() ?>panel/selsucursal/"+$(this).val()+"/"+$(this).find('option:selected').html();
    });
</script>