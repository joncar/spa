<?php if(empty($scripts)): ?>
<link type="text/css" rel="stylesheet" href="<?= base_url('assets/grocery_crud/css/jquery_plugins/bootstrap.datepicker/bootstrap.datepicker.css') ?>" />
<script src="<?= base_url('assets/grocery_crud/js/jquery_plugins/bootstrap.datepicker.js') ?>"></script>
<script src="<?= base_url('assets/grocery_crud/js/jquery_plugins/config/jquery.datepicker.config.js') ?>"></script>
<?php endif ?>
<script>
$(function(){    
    date_init_calendar();
});

function date_init_calendar(){
    $('.datetime-input').datepicker({
            format:    "dd/mm/yyyy",
            autoclose: true,
            /*startDate: new Date(),*/
            weekStart: 1
    });
}
</script>